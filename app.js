
$(document).ready(function () {
    $("a.nav-link").click(function (event) {
        event.preventDefault();
        $("html, body").animate({ scrollTop: $($(this).attr("href")).offset().top - 100 }, 1000);
    });

    setTimeout(function () {
        $('#signupModal').modal('show');
    }, 2000);

});

// lazy load images
const observer = lozad();
observer.observe();

//google maps API

var map;

function initMap() {

    // The map, centered at zip from client input(latlng)
    var bounds = new google.maps.LatLngBounds();
    var latlng = new google.maps.LatLng(39.769137, -105.024305);
    map = new google.maps.Map(
        document.getElementById('map'), {
            zoom: 13,
            center: latlng
        })

    marker = new google.maps.Marker({
        position: latlng,
        map: map,
        title: 'La Guapa Boutique'
    });

};